export interface Tab {
    active: boolean,
    height: number,
    highlighted: boolean,
    id: number,
    index: number,
    mailTab: boolean,
    status: string,
    title: string,
    type: string,
    url: string,
    width: number,
    windowId: number,
}

export interface Browser {
    tabs: {
        create: Function,
        update: Function,
        query: Function,
        sendMessage: Function
    },
    storage: {
        local: {
            get: Function,
            set: Function,
        },
        onChanged: {
            addListener: Function,
            removeListener: Function
        }
    },
    i18n: {
        getMessage: Function
    },
    spacesToolbar: {
        addButton: Function,
        removeButton: Function,
        updateButton: Function
    },
    browserAction: {
        setIcon: Function
    }
}

export interface StoredDarkModeGCalTab {
    darkModeGCalTab: { oldValue: boolean, newValue: boolean } | boolean;
}

export interface StoredSpacesToolbarIconGCalTab {
    spacesToolbarIconGCalTab: { oldValue: boolean, newValue: boolean } | boolean;
}

export const IconStyle = {
    blue: "blue",
    dark: "dark",
    light: "light"
} as const;

export type IconStyleType = typeof IconStyle[keyof typeof IconStyle];

export interface StoredPreferredIconGCalTab {
    preferredIconGCalTab: { oldValue: IconStyleType, newValue: IconStyleType } | IconStyleType;
}

export interface DarkReaderApi {
    enable: Function,
    disable: Function,
    isEnabled: Function,
    setFetchMethod: Function
}
