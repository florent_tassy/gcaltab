import DarkReader from "darkreader";
import type { 
    Browser, 
    StoredDarkModeGCalTab, 
    StoredSpacesToolbarIconGCalTab 
} from "./interfaces";
import { retrieveLocalStorageValue } from "./functions";

// Define a synonym for browser so that Typescript does not 
// panic.
//@ts-ignore
let browserApi: Browser = browser;

/** Function that activates DarkReader.
* */
function activateDarkReader() {
    DarkReader.setFetchMethod(window.fetch);
    DarkReader.enable({
        brightness: 120,
        contrast: 90,
        sepia: 10,
    });
}

/** Function that inactivates DarkReader.
 * */
function inactivateDarkReader() {
    DarkReader.disable();
}

/** Function that enables or disables DarkReader */
export function switchDarkReader(darkMode: boolean) {
    if (darkMode) {
        activateDarkReader();
    }
    if (!darkMode) {
        inactivateDarkReader();
    }
}

/** Initialize DarkReader as per local storage value */
retrieveLocalStorageValue().then(storedDarkMode => {
    if (typeof storedDarkMode.darkModeGCalTab === "boolean") {
        switchDarkReader(storedDarkMode.darkModeGCalTab);
    }
});

/** Listen to localStorage changes */
browserApi.storage.onChanged.addListener(onLocalStorageItemChanged);

/** Function that reacts to local storage changes.
 * Called if local storage values changed and were retrieved successfully.
 * */
function onLocalStorageItemChanged(item: StoredDarkModeGCalTab | StoredSpacesToolbarIconGCalTab) {
    if ("darkModeGCalTab" in item) {
        let darkMode: boolean = typeof item.darkModeGCalTab === "object" ? item.darkModeGCalTab.newValue : item.darkModeGCalTab;
        switchDarkReader(darkMode);
    }
}