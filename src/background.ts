import type { 
    Browser, 
    StoredDarkModeGCalTab, 
    StoredPreferredIconGCalTab, 
    StoredSpacesToolbarIconGCalTab 
} from "./interfaces";
import { 
    retrieveLocalStorageSpacesToolbarIconValue
} from "./functions";

// Define a synonym for browser so that Typescript does not 
// panic.
//@ts-ignore
let browserApi: Browser = browser;

const url: string = "https://www.google.com/calendar";
const spacesToolbarButtonId: string = "gcaltab_" + (Math.floor(Math.random() * 99999999)).toString();
let showSpacesToolbarButton: boolean = false;
let iconPath: string = "icons/gcaltab.svg";

/** Function that reacts to local storage changes.
 * Called if local storage values changed and were retrieved successfully.
 * */
function onLocalStorageItemChanged(item: StoredDarkModeGCalTab | StoredSpacesToolbarIconGCalTab | StoredPreferredIconGCalTab) {
    if ("spacesToolbarIconGCalTab" in item) {
            addOrRemoveSpacesToolbarButton(typeof item.spacesToolbarIconGCalTab === "object" ? 
                item.spacesToolbarIconGCalTab.newValue : 
                item.spacesToolbarIconGCalTab);
    }
    if("preferredIconGCalTab" in item) {
        setIcon();
    }
}

/** Function that get i18n label for GCalTab button */
function getLabel(): string {
    return browserApi.i18n.getMessage("gCalButton");
}

/** Function that add or remove spaces toolbar icon */
function addOrRemoveSpacesToolbarButton(show: boolean): void {
    if (!show) {
        browserApi.spacesToolbar.removeButton(spacesToolbarButtonId);
    }

    if (show) {
        browserApi.spacesToolbar.addButton(
            spacesToolbarButtonId,
            getButtonProperties()
            );
        console.info("GCalTab spaces toolbar menu created.");
    }

    showSpacesToolbarButton = show;
    return;
}

/** Function that update spaces toolbar button if the API exists */
function refreshSpacesToolbar(): void {
    try {
        if (showSpacesToolbarButton) {
            browserApi.spacesToolbar.updateButton(
                spacesToolbarButtonId, 
                getButtonProperties()
            );
        }
        console.log("GCalTab spaces toolbar menu refreshed.");
    } catch (e: any) {
        console.log("spacesToolbar is not defined...\n", e);
    }
}

/* Function that changes the add-on icon */
function setIcon(): void {
    if (browserApi.browserAction.setIcon) {
        let getPreferredIcon: any = browserApi.storage.local.get("preferredIconGCalTab")

        getPreferredIcon.then((storedValue: any) => {
            const preferredIcon: string = storedValue["preferredIconGCalTab"];
            if (preferredIcon) {
                iconPath = `icons/gcaltab-${storedValue["preferredIconGCalTab"]}.svg`;
                browserApi.browserAction.setIcon({ path: iconPath });
                refreshSpacesToolbar();
            } else {
                browserApi.storage.local.set({ "preferredIconGCalTab": "blue" });
            }
        });
    }
}

/* Wrapper function to get spaces toolbar button properties */
function getButtonProperties(): Object {
    return {
        defaultIcons: {
            16: iconPath,
            32: iconPath
        },
        title: getLabel(),
        url: url
    }
}

/* Initialization sequence */

/** Initialize icon style as per local storage value */
setIcon();

/** Initialize spaces toolbar icon as per local storage value */
retrieveLocalStorageSpacesToolbarIconValue().then(storedItem => {
    if (typeof storedItem.spacesToolbarIconGCalTab === "boolean") {
        addOrRemoveSpacesToolbarButton(storedItem.spacesToolbarIconGCalTab);
    }
});

/** Listen to localStorage change */
browserApi.storage.onChanged.addListener(onLocalStorageItemChanged);
