import type { 
    StoredDarkModeGCalTab, 
    StoredSpacesToolbarIconGCalTab, 
    Browser, 
    StoredPreferredIconGCalTab
} from "./interfaces";

// Define a synonym for browser so that Typescript does not 
// panic.
//@ts-ignore
let browserApi: Browser = browser;

/** Function that retrieves darkModeGCalTab from local storage.
 * */
export async function retrieveLocalStorageValue(): Promise<StoredDarkModeGCalTab> {
    let gettingItem: StoredDarkModeGCalTab = await browserApi.storage.local.get("darkModeGCalTab");
    return gettingItem;
}

/** Function that retrieves spacesToolbarIconGCalTab from local storage.
 * */
 export async function retrieveLocalStorageSpacesToolbarIconValue(): Promise<StoredSpacesToolbarIconGCalTab> {
    let gettingItem: StoredSpacesToolbarIconGCalTab = await browserApi.storage.local.get("spacesToolbarIconGCalTab");
    return gettingItem;
}

/** Function that retrieves preferredIcon from local storage.
 * */
export async function retrieveLocalStoragePreferredIconValue(): Promise<StoredPreferredIconGCalTab> {
    let gettingItem: StoredPreferredIconGCalTab = await browserApi.storage.local.get("preferredIconGCalTab");
    return gettingItem;
}
