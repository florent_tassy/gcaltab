![Thunderbird add-on rating](https://img.shields.io/badge/dynamic/xml?style=flat-square&color=success&label=⭐&query=%2F%2Faside%2Fdiv%5B1%5D%2Fspan&url=https%3A%2F%2Faddons.thunderbird.net%2Fen%2Fthunderbird%2Faddon%2Fgcaltab%2F)
![Thunderbird add-on users](https://img.shields.io/badge/dynamic/xml?style=flat-square&color=success&label=👥&query=%2F%2Fdiv%5B%40id%3D%27daily-users%27%5D&url=https%3A%2F%2Faddons.thunderbird.net%2Fen%2Fthunderbird%2Faddon%2Fgcaltab%2F)
![License](https://img.shields.io/gitlab/license/37593453?style=flat-square)
![Thunderbird add-on version](https://img.shields.io/badge/dynamic/xml?style=flat-square&color=informational&label=version&query=%2F%2Fspan[%40class%3D%27version-number%27]&url=https%3A%2F%2Faddons.thunderbird.net%2Fen%2Fthunderbird%2Faddon%2Fgcaltab%2F)

<img src="public/icons/gcaltab-blue.svg" alt="GCalTab icon" height="150">

# GCalTab
GCalTab is a Thunderbird add-on that opens Google Calendar website in a Thunderbird tab. It facilitates multiple accounts management and features a beautiful dark mode thanks to the [DarkReader](https://darkreader.org/) algorithm.  

**[![Get the add-on](https://raw.githubusercontent.com/thundernest/addon-developer-support/6a64a4698f7e96f014fe1544d426c92e47102b0e/images/get-the-addon.svg)](https://addons.thunderbird.net/thunderbird/addon/gcaltab)**

## How does it work ?
Once the add-on is installed, a new icon appears in Thunderbird Mail toolbar:
![Thunderbird Mail toolbar](images/GCalTab_browser_action.png)

Click on it to see GCalTab menu:  
<img src="images/GCalTab_popup.png" alt="Thunderbird menu">

Click on "Open" to open the Google Calendar tab:  
<img src="images/Google_Calendar_dark.png" alt="Thunderbird menu">

You can switch on/off the "Dark" button to activate DarkReader on the Google Calendar tab.

### There are plenty of add-ons that open a Google Calendar tab in Thunderbird. What does GCalTab bring ?
GCalTab brings two things that are often missed in other add-ons:  
* The possibility to switch to a dark mode. Save your eyes !
* The possibility to simply disconnect - reconnect and switch between several accounts from GCalTab menu.  

### What permissions are needed ?
GCalTab will need to access your data for:  
* accounts.google.com  
* calendar.google.com  
* google.com  
* keep.google.com  
* tasks.google.com
* gstatic.com
* fonts.googleapis.com

In addition, GCalTab will need to access Thunderbird tabs to maintain a unique tab.

### Does it mean that GCalTab collects my data ?
No, GCalTab simply opens tabs in Thunderbird, exactly as it would be done in a web browser.  
It does not collect any data.  
These permissions are needed to allow GCalTab to "manage" tabs for these three domains in Thunderbird. "Manage" means:
* opening tabs
* execute scripts on these tabs

Tabs are open for obvious reason: that's the aim of the add-on.  
The only script that can be executed on these tabs is the [DarkReader](https://darkreader.org/) algorithm, that has a strict [privacy policy](https://darkreader.org/privacy/).  

Of course, Google Calendar works as it would do in your web browser, including regarding data collection.  

### So GCalTab will never collect my data ?
Never.

### How do I stay connected when I re-open Thunderbird ?
If you wish to stay connected to Google Calendar, you will have to allow cookies.  
The cookies menu can be accessed as follows in Thunderbird:    
_GNU/Linux_ : "Edit" > "Preferences" > "Privacy" or "Privacy and Security" > "Web Content" section.  
_macOS_ : "Thunderbird" > "Preferences" > "Privacy" or "Privacy and Security" > "Web Content" section.  
_Windows_ : "Tools" > "Options" > "Privacy" or "Privacy and Security" > "Web Content" section.  
You will have to allow and keep cookies for these three domains:
* accounts.google.com
* calendar.google.com
* google.com

### How do I disconnect from a Google Calendar account ?
If you use the Google Calendar "Sign out" button, the link will open in your web browser and will not disconnect Thunderbird but your web browser session.  
**Since version 0.5**, it is possible to sign out from GCalTab menu by pushing the "Sign out" button <img src="images/sign-out-alt-solid.png" alt="Sign out icon" height="16px">.  

### How do I switch to another Google Calendar account ? 
If you use the Google Calendar "Add account" or "Manage your Google Account" button, the link will open in your web browser and will not change your Thunderbird session.  
**Since version 0.7**  
If you have ever connected to the account you wish to switch to, you can click the "Switch account" button <img src="images/user-friends-solid.png" alt="Switch account icon" height="16px">.  
If you wish to connect to a new account, this can be done by clicking the "Add account" button <img src="images/user-plus-solid.png" alt="Add account icon" height="16px">.  

## Build from source

Building the add-on from source requires to have a working [Node.js](https://nodejs.org). In addition, some package.json scripts would work only in a "*nix" environment. The add-on can be built by running the following command from its root directory:  
```
npm ci
npm run build:addon
```

## Legal notice
Thunderbird is a registered trademark of the Mozilla Foundation.  
Google is a registered trademark of Google LLC.  
GNU is a registered trademark of the Free Software Foundation.  
Linux is a registered trademark of Linus Torvalds.  
Windows is a registered trademark of Microsoft Corporation.  
macOS is a registered trademark of Apple Inc.  
Node.js is a trademark of the OpenJS Foundation.  

The above-mentioned trademarks are only used to refer to products.  
GCalTab and its developer are not affiliated, sponsored nor endorsed by any of the above-mentioned organizations.  

## Acknowledgments
GCalTab uses Font Awesome icons available under [Creative Commons Attribution 4.0 International license](https://fontawesome.com/license).

## Changelog
3.1.0 -> upgrade dependencies, fix README, fix spaces toolbar not initialized at Thunderbird start, fix `npm run dev` command, fix permissions  
3.0.0 -> upgrade dependencies, new SVG icons, icon switcher, correct README and add badges, add CI/CD configuration, many tiny fixes  
2.2.0 -> upgrade dependencies, update npm commands, refresh and beautify README  
2.1.0 -> upgrade dependencies, add spaces toolbar icon for Thunderbird >= 100  
2.0.0 -> complete re-write using [Svelte](https://svelte.dev/) and [TypeScript](https://www.typescriptlang.org/), upgrade to DarkReader v4.9.51, upgrade to web-ext 7.1.0  
1.2.4 -> override indirect dev dependency to fix security issue   
1.2.3 -> upgrade to DarkReader v4.9.46, upgrade to web-ext 6.8.0  
1.2.2 -> upgrade to DarkReader v4.9.44, upgrade to web-ext 6.7.0, popup style adjustments  
1.2.1 -> upgrade to web-ext 6.6.0  
1.2.0 -> switch from Spectre.css to home-made CSS for popup, upgrade to DarkReader v4.9.40, upgrade to web-ext v6.5.0  
1.1.2 -> upgrade to DarkReader v4.9.35, upgrade to web-ext 6.4.0  
1.1.1 -> updated indirect dependencies versions due to security fixes  
1.1.0 -> addition of i18n, upgrade to DarkReader v4.9.33, changed manifest so that DarkReader applies to Keep and Tasks  
1.0.0 -> npm is now used to manage 3rd party libraries, upgrade to DarkReader v4.9.16 and Spectre.css v0.5.9  
0.7 -> addition of buttons to ease multiple accounts management  
0.6 -> upgrade to DarkReader v4.9.10  
0.5 -> addition of the disconnection button  
0.4 -> first release  
